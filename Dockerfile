FROM mcr.microsoft.com/dotnet/core/sdk:3.1-alpine AS compiler
WORKDIR /app

ADD src/ .

RUN dotnet publish -f netcoreapp3.1 -o /app/dist

FROM mcr.microsoft.com/dotnet/core/runtime:3.1-alpine AS runtime
WORKDIR /app

COPY --from=compiler /app/dist .

ENTRYPOINT [ "dotnet", "test-sql-provider.dll" ]
