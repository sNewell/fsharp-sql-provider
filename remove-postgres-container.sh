#!/bin/sh

# Stops and removes the container and also remoes the volume

docker stop cat-postgres-db
docker rm cat-postgres-db
docker volume rm cat-db

echo "No more dockerized cat data"
