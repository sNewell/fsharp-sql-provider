#!/bin/sh

# Find nuget's global directory
nugetRootPath=$(dotnet nuget locals global-packages -l | cut -d' ' -f4)
nugetRootPath="${nugetRootPath%?}"

# 'array' (space delimited) of ${nameOfDep}|${pathToDllFolder}
packagesAndPaths="System.Runtime.CompilerServices.Unsafe|4.6.0/lib/netstandard2.0"

mkdir -p lib

echo "$packagesAndPaths" | tr ' ' '\n' | while read i; do
  # all these vars split out for clarity
  name=$(echo "$i" | cut -d'|' -f1)
  path=$(echo "$i" | cut -d'|' -f2)
  nugetDll="$name.dll"
  pathFromNugetRoot="$(echo "$name" | awk '{print tolower($0)}')/$path"
  fullPath="$nugetRootPath/$pathFromNugetRoot/$nugetDll"

  cp "$fullPath" "./lib/$nugetDll"
  echo "  > Copied $nugetDll into lib for type provider."
done
