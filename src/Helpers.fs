/// Abstract functional heplers that don't go anywhere particularly specific
module Helpers

/// Take a function, and a tuple, and call the function with the destructured tuple
let withTuple f (a, b) = f a b

/// Take a func, and a triple, and call the func with the destructured tuple
let withTriple f (a, b, c) = f a b c

/// Take anything, and give back a zero - a success exit code
let returnZero _ = 0

/// Takes a function and its param and wraps it in another function,
/// useful for if you need unit -> whatever or need to defer work
let thunkOne f x = (fun _ -> f x)
