module Cli

open Domain

module DB = DbAccess

/// Gets a string from the console plain an' simple (user presses enter)
let private getInput() = System.Console.ReadLine()

/// Gets an integer from the console, keeps asking the user until they do the right thing
let rec private getIntInput() =
  match getInput() |> System.Int32.TryParse with
  | true, parsedInt -> parsedInt
  | _ ->
      printf "  (oops, expected an integer like 5 or 10!)\n\t> "
      getIntInput()

let commandList =
  [ ("help", "?")
    ("count", "c")
    ("add", "a")
    ("quit", "q") ]
  |> List.map (fun (cmd, abbrev) -> sprintf "%s (%s)" cmd abbrev)
  |> String.concat "
	"

let private handleAdd() =
  printf "What do you want to add? (cat(c) | breed(b))
  > "
  let addType = getInput()
  match addType.ToLowerInvariant() with
  | "c"
  | "cat" ->
      printf "  > What's the kitty's name? > "
      let name = getInput()
      printf "  > How old is the kitty? > "
      let age = getIntInput()
      printf "  > What's the kitty's breed? > "
      let breed = getInput()
      let newCat = mkCat name age breed |> DB.insertCat
      catToStr newCat |> printfn "  > There's a new cat on the block: %s"
  | "b"
  | "breed" ->
      printf "  > What's the breed? > "
      let breed = getInput()
      match DB.findBreedIdByName breed with
      | Some id -> printfn "  > That breed already exists with id %d" id
      | None -> DB.insertBreed breed |> printfn "  > Inserted breed, it has id %d"
  | _ -> printfn "Wat? Idk what that has to do with kittehs."
  true

/// Handles input - matches on lowered string and returns to keep going or not
let private handleInput (str: string) =
  let lowered = str.ToLowerInvariant()
  match lowered with
  | "?"
  | "help" ->
      printfn "Available Commands are:
	%s"   commandList
      true
  | "c"
  | "count" ->
      let latestCatNumber = DB.countNumOfCats()
      printfn "Currently tracking %d cats" latestCatNumber
      true
  | "a"
  | "add" -> handleAdd()
  | "q"
  | "quit" -> false
  | _ -> true

let private prompt() = printf " > "

let start() =
  let latestCatNumber = DB.countNumOfCats()
  printfn "Cat Manager CLI 😺: Currently tracking %d cats" latestCatNumber
  let mutable keepGoing = true
  while keepGoing do
    keepGoing <-
      ()
      |> prompt
      |> getInput
      |> handleInput
  printfn "You're purrfect! 😽"
  0
