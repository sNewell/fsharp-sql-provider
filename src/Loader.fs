/// Load data into the db the easy way!
module Loader

open Domain
open Helpers

module DB = DbAccess

/// Take a tuple and make a cat in the db, then turn it into a string
let private tupleToCatStr =
  withTriple mkCat
  >> DB.insertCat
  >> catToStr

let private rawCatData =
  [ ("Denton", 2, "Gray Tabby")
    ("Mitzie", 6, "Tuxedo")
    ("Saphire", 1, "Blue Russian")
    ("Ailee", 2, "Siamese")
    ("Oreo", 6, "Tuxedo")
    ("Frisky", 15, "Tuxedo") ]

/// Loads a set of data into the db
let load() =
  rawCatData
  |> List.map tupleToCatStr
  |> String.concat ", "
  |> printfn "Made a bunch o cats!
***
%s
***
"
  |> returnZero
