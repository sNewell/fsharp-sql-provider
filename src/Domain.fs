/// Domain modelling - normal F#!
module Domain

/// A type of cat (ignoring attributes for now)
type Breed =
  { id: int option
    name: string }

/// KITTEH
type Cat =
  { id: int option
    name: string
    age: int
    breed: Breed }

/// Make a cat with just a name and breed name, with None for ids
let mkCat catName age breedName =
  { id = None
    age = age
    name = catName
    breed =
      { id = None
        name = breedName } }

/// Turn a cat to a string - my how the turn tables!
let catToStr cat = sprintf "%s(%d)" cat.name (Option.defaultValue -1 cat.id)
