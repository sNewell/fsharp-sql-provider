/// Encapsulates all db access, exposes domain types
module DbAccess

open Domain
open FSharp.Data.Sql
open Helpers

[<Literal>]
let private DbVendor = Common.DatabaseProviderTypes.POSTGRESQL

#if CI
[<Literal>]
let private PostgresHost = "127.0.0.1"
#else
[<Literal>]
let private PostgresHost = "localhost"
#endif

[<Literal>]
let private ConnString = "Host=" + PostgresHost + ";Port=5432;Database=postgres;Username=test_app;Password=pass"

/// Pull runtime connection string form DB_CONN_STR env var
let private runtimeConnectionString =
  let rawEnvStr = System.Environment.GetEnvironmentVariable("DB_CONN_STR")
  if System.String.IsNullOrWhiteSpace(rawEnvStr)
  then ConnString
  else rawEnvStr.Trim()

[<Literal>]
let private Schema = "test"

[<Literal>]
let private ResPath = "/home/sean/repos/fsharp-sql-provider/src" + "./lib"

[<Literal>]
let private IndivAmount = 100

[<Literal>]
let private UseOptTypes = true

type private DB =
  SqlDataProvider<DatabaseVendor=DbVendor, ConnectionString=ConnString, ResolutionPath=ResPath, IndividualsAmount=IndivAmount, UseOptionTypes=UseOptTypes, Owner=Schema>

let private ctx = DB.GetDataContext(runtimeConnectionString, SelectOperations.DatabaseSide)
let private catDb = ctx.Test
let private Attributes = catDb.Attributes
let private Cats = catDb.Cats
let private Breeds = catDb.Breeds
let private Owners = catDb.Owners
let private BreedAttrs = catDb.BreedAttributes
let private OwnerCats = catDb.OwnerCats
let private createBreed = Breeds.``Create(name)``
let private createAttribute = Attributes.``Create(description, name)``
let private createCat = Cats.``Create(age, breedid, name)``
let private createOwner = Owners.``Create(age, name)``
let private createBreedAttr = BreedAttrs.``Create(attributeid, breedid)``
let private createOwnerCat = Owners.``Create(age, name)``

/// Query db for a breedId, by Breed Name
let findBreedIdByName breedName =
  query {
    for b in Breeds do
      where (b.Name = breedName)
      take 1
      select b.Id
  }
  |> Seq.tryHead

/// Insert a breed into the db by name
let insertBreed breedName =
  let breedEntity = createBreed (breedName)
  printfn "    > Makin' breed %s..." breedName
  ctx.SubmitUpdates()
  printfn "    > ✅"
  breedEntity.Id

/// Deferred insert Breed
let private defInsertBreed: string -> unit -> int = thunkOne insertBreed

/// Insert a cat into the db, will insert the breed if necessary
let insertCat cat =
  let breedId = findBreedIdByName cat.breed.name |> Option.defaultWith (defInsertBreed cat.breed.name)
  let catEntity = createCat (cat.age, breedId, cat.name)
  printfn "  > Makin' kitteh %s" cat.name
  ctx.SubmitUpdates()
  printfn "  > ✅"
  { cat with
      id = Some catEntity.Id
      breed = { cat.breed with id = Some breedId } }

let countNumOfCats() =
  query {
    for _ in Cats do
      select true
      count
  }
