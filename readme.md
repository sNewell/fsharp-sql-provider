# FSharp SQLProvider + Cats

[![pipeline status](https://gitlab.com/sNewell/fsharp-sql-provider/badges/master/pipeline.svg)](https://gitlab.com/sNewell/fsharp-sql-provider/pipelines)

This explores the use of an F# Type Provider - SQLProvider - with the postgres database.
This is just to write down a full tutorial of how to incorporate type providers in your project,
and hopefully add some ideas on how to make the experience flawless for wherever your code goes!
