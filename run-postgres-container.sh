#!/bin/sh

echo "Setting up postgres db in a docker container with a volume..."

if docker volume ls | grep cat-db > /dev/null
then
 echo '  > volume cat-db found'
else
 echo '  > creating volume cat-db'
  docker volume create cat-db
fi

echo "starting cat-postgres-db in the background with cat-db volume..."

docker run --name cat-postgres-db \
  -e POSTGRES_PASSWORD=pass \
  -d \
  -p 5432:5432 \
  -v cat-db:/var/lib/postgresql/data \
  postgres:12.1-alpine

echo 'giving postgres a hot minute to boot up...'
sleep 4

echo "creating db user and schema..."

cat pre-schema.sql | docker exec -i cat-postgres-db \
  psql -U postgres -f -

echo "loading schema..."

cat schema.sql | docker exec -i cat-postgres-db \
  psql -U test_app -d postgres -f -

echo "cat-postgres-db setup on localhost:5432"
