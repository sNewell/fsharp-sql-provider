module HelperTests

open FsCheck
open Helpers
open NUnit.Framework

/// Quick Check - Quick Throw On Failure
let QC = Check.QuickThrowOnFailure

let isAlwaysZero x = (returnZero x) = 0

[<Test>]
let ``Is always 0``() = QC isAlwaysZero

let thunkReturnsTheSameThing f x = ((thunkOne f x)()) = f x

[<Test>]
let ``Thunk never changes the result``() = QC thunkReturnsTheSameThing

let callsFuncWithDestructuredTuple f tup = (withTuple f tup) = f (fst tup) (snd tup)

[<Test>]
let ``Calls Tuples correctly``() = QC callsFuncWithDestructuredTuple

let callsTripleEquivalently f (a, b, c) = (withTriple f (a, b, c)) = f a b c

[<Test>]
let ``Calls Triples correctly``() = QC callsTripleEquivalently
