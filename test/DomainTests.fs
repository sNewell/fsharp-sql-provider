module DomainTests

open Domain
open FsUnitTyped
open NUnit.Framework

[<Test>]
let ``Cat defaults with negative one id``() =
    mkCat "meow" 5 "test"
    |> catToStr
    |> shouldEqual "meow(-1)"

[<Test>]
let ``Cat shows id``() =
    { id = Some 100
      age = 10
      name = "friskie"
      breed =
          { id = None
            name = "" } }
    |> catToStr
    |> shouldEqual "friskie(100)"
