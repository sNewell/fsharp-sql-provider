#!/bin/sh

echo "CREATE SCHEMA IF NOT EXISTS test AUTHORIZATION test_app;" \
  | PGPASSWORD=pass psql -U test_app -d postgres -f -

cat /schema.sql | PGPASSWORD=pass psql -U test_app -d postgres -f -
