# GitLab Runner Config

The `values.yaml` file here sets the configuration values for the gitlab helm chart. This
sets up our private CI infrastructure, with support for services like postgres.

A few notes on this particular set of values:

- Leverages [kaniko](https://docs.gitlab.com/ee/ci/docker/using_kaniko.html) to build docker images in the pipeline
- Allows services to be used
- Limits concurrency to a configurable amount

## Example command

If you have a `.env` file setup with something like:

```sh
CI_TOKEN=your_gitlab_token
```

you can do:

```sh
source .env

# first time install
helm install -n YOUR_NAMESPACE YOUR_RELEASE_NAME -f values.yaml gitlab/gitlab-runner --set runnerRegistrationToken=$CI_TOKEN

# upgrade
helm upgrade -n YOUR_NAMESPACE YOUR_RELEASE_NAME -f values.yaml gitlab/gitlab-runner --set runnerRegistrationToken=$CI_TOKEN
```

And you'll have a fleet of runners setup in a namesapce, with the given release name, loaded with the values specified
in here.
